let assert = require('assert');
let rq = require('request-promise');

describe('System Test', function() {
  describe('#Foobar Response', function() {
    it('system should respond correctly', function(done) {
      rq.get('http://dang-app-a-staging.westeurope.cloudapp.azure.com:8081/foobar')
      .then(result => {
        assert.equal(JSON.parse(result).value, 'foobar')
        done()
      }).catch( error => {
        done(error)
      })
    });
  });

  describe('#App-B Test', function() {
    it('should return correct value', function(done) {
      rq.get('http://dang-app-b-staging.westeurope.cloudapp.azure.com:8082/bar')
      .then(result => {
        assert.equal(result, 'bar')
        done()
      }).catch( error => {
        done(error)
      })
    });

    it('should say hello', function(done) {
      rq.get('http://dang-app-b-staging.westeurope.cloudapp.azure.com:8082/hello')
      .then(result => {
        assert.equal(result, 'hello world')
        done()
      }).catch( error => {
        done(error)
      })
    });
  });
});